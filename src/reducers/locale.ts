import { Action, ActionTypes } from '../actions';

export const langReducer = (state: string = 'en', action: Action) => {
  switch (action.type) {
    case ActionTypes.changeLang:
      return action.payload;
    default:
      return state;
  }
};
