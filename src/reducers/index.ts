import { combineReducers } from 'redux';
import { userReducer } from './user';
import { IUser } from '../actions';
import { langReducer } from './locale';

export interface IStoreState {
  persons: IUser[];
  lng: string;
}

export const reducers = combineReducers<IStoreState>({
  persons: userReducer,
  lng: langReducer
});
