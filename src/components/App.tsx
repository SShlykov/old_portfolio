import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import MainMenu from './menu';
import MainPage from './pages/MainPage';
import NotFound from './pages/NotFound';
import PageContainer from './containers/pageContainer';
import SergeyPage from './pages/SergeyPage';

import './app.sass';
import Construction from './pages/Construction';
class Component extends React.Component<{ location: { pathname: string } }> {
  state = {
    construction: false
  };
  handleWindowResize = () => {
    if (window.innerWidth < 350) {
      this.setState({ construction: true });
    } else {
      this.setState({ construction: false });
    }
  };
  componentDidMount = (): void => {
    this.handleWindowResize();
    window.addEventListener('resize', this.handleWindowResize);
  };
  componentWillUnmount = (): void => {
    window.removeEventListener('resize', this.handleWindowResize);
  };
  render() {
    if (this.state.construction) {
      return (
        <PageContainer>
          <Construction />
        </PageContainer>
      );
    }
    return (
      <PageContainer>
        <MainMenu location={this.props.location} />
        <Switch>
          <Route path='/' component={MainPage} exact />
          <Route path='/sergey' component={SergeyPage} exact />
          <Route component={NotFound} />
        </Switch>
      </PageContainer>
    );
  }
}
const App = withRouter(props => <Component {...props} />);
export { App };
