import React from 'react';

const Container = (props: { children: any }) => {
  return <div className='mp-container'>{props.children}</div>;
};

export default Container;
