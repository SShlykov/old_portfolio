import React from 'react';
import { Link } from 'react-router-dom';
import arrow from './arrows.svg';
import portfolio from './portfolio.svg';
import tools from './tools.svg';

import './mainmenu.sass';
import { smoothScrollToId } from '../../utils/smoothscroll';
import LangSwither from './../../utils/LangSwitcher';

interface IMainMenuProps {
  location: { pathname: string };
}

const MenuTitle = () => {
  return <div className='main-navbar__title'>portfolio</div>;
};

class MainMenu extends React.Component<IMainMenuProps> {
  state = {
    isOpened: false,
    menu: [
      { pic: portfolio, text: 'Resume' },
      { pic: tools, text: 'Projects' }
    ],
    menuState: 'top'
  };
  onChange = () => this.setState({ isOpened: !this.state.isOpened });
  handlePosition = () => {
    let topPos = window.scrollY;

    let pageHeight =
      //@ts-ignore
      document.getElementsByClassName('main-content')[0].offsetHeight;
    if (topPos <= 50) this.setState({ menuState: 'top' });
    else if (topPos > pageHeight - 1150) this.setState({ menuState: 'bottom' });
    else if (topPos > 50) this.setState({ menuState: 'middle' });
  };
  componentDidMount = () => {
    this.handlePosition();
    window.addEventListener('scroll', this.handlePosition);
  };
  render() {
    const { location } = this.props;
    return (
      <nav
        className={
          this.state.isOpened
            ? `main-navbar menu-${this.state.menuState} opened`
            : `main-navbar menu-${this.state.menuState}`
        }
      >
        <MenuTitle />
        <ul>
          {location.pathname.slice() !== '/' ? (
            <span>
              <Link to='/' style={{ textDecoration: 'none' }}>
                {'< back'}
              </Link>
            </span>
          ) : (
            ''
          )}
          <li className='open-menu' onClick={this.onChange}>
            <img src={arrow} alt='' />
          </li>

          {this.state.menu.map(el => {
            return (
              <li
                key={el.text}
                onClick={() => smoothScrollToId(el.text.toLowerCase())}
              >
                <img src={el.pic} alt='' />
                <span>{el.text}</span>
              </li>
            );
          })}

          <LangSwither />
        </ul>
      </nav>
    );
  }
}

export default MainMenu;
