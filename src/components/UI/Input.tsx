import React from 'react';
import InlineError from './messages/InlineError';

interface IInputProps {
  name: string;
  label: string;
  children?: any;
  type?: string;
  onChange?: any;
  error?: string;
}

export default function Input(props: IInputProps) {
  let type: string = props.type ? props.type : 'text';
  let id: string = `${props.label}${type}`;
  return (
    <div className={!props.error ? 'input' : 'input error'}>
      <input
        type={type}
        value={props.children}
        placeholder={props.label}
        name={props.name}
        id={id}
        onChange={props.onChange}
        disabled
      />
      <label htmlFor={id}>{props.label}</label>
      <span className='border'></span>
      {props.error ? <InlineError text={props.error} /> : ''}
    </div>
  );
}
