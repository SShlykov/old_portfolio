import React from 'react';
import InlineError from './messages/InlineError';

interface ITextareaProps {
  name: string;
  label: string;
  children?: any;
  type?: string;
  onChange?: any;
  error?: string;
}

class TextArea extends React.Component<ITextareaProps> {
  state = {
    height: 36
  };
  resizeTextArea = (e: any) => {
    e.target.style.height = 'auto';
    let height = e.target.scrollHeight;
    e.target.style.height = height + 'px';
    if (height < 50) {
      height = 36;
    }
    this.setState({ height: height });
  };
  render() {
    let type: string = this.props.type ? this.props.type : 'text';
    let id: string = `${this.props.label}${type}`;
    return (
      <div
        className={
          !this.props.error ? 'input textarea' : 'input textarea error'
        }
        style={{ height: `${this.state.height + 5}px` }}
      >
        <textarea
          value={this.props.children}
          placeholder={this.props.label}
          name={this.props.name}
          id={id}
          onChange={this.props.onChange}
          onInput={this.resizeTextArea}
        />
        <label htmlFor={id}>{this.props.label}</label>
        <span className='border'></span>
        {this.props.error ? <InlineError text={this.props.error} /> : ''}
      </div>
    );
  }
}

export default TextArea;
