import React from 'react';

interface IInputGroupProps {
  children?: any;
}

export default function InputGroup(props: IInputGroupProps) {
  return <div className='input-group'>{props.children}</div>;
}
