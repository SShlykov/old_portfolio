import React, { FormEvent } from 'react';
import { Input, InputGroup, TextArea } from './../../UI';
import { smoothScroll } from '../../../utils/smoothscroll';
import Validator from 'validator';

export interface IFeedbackFormProps {
  children?: any;
  title: string;
  subtitle?: string;
  sended: boolean;
  submit: (data: IFormData) => void;
}
export interface IFormData {
  name: string;
  contact: string;
  message: string;
}
export interface IFormErrors {
  name?: string;
  contact?: string;
  message?: string;
}

class FeedbackForm extends React.Component<IFeedbackFormProps> {
  state = {
    data: { name: '', contact: '', message: '' },
    loading: false,
    errors: { name: undefined, contact: undefined, message: undefined }
  };
  onChange = (e: { target: { name: string; value: string } }) =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  onSubmit = (e: FormEvent) => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors: errors });
    if (Object.keys(errors).length === 0) {
      this.props.submit(this.state.data);
    }
  };
  isDataEmpty = (data: IFormData) => {
    let errors: IFormErrors = {};
    Object.keys(data).forEach(el => {
      //@ts-ignore
      if (Validator.isEmpty(data[el])) errors[el] = `${el} can't be blank`;
    });
    return errors;
  };
  validate = (data: IFormData) => {
    let errors: IFormErrors = {};
    errors = this.isDataEmpty(data);
    if (!Validator.isEmail(data.contact)) errors.contact = 'Invalid email';
    if (!Validator.isLength(data.name, { min: 3, max: 20 }))
      errors.name = 'Name should be between 3 and 20 symbols';
    if (!Validator.isLength(data.message, { min: 10, max: 500 }))
      errors.message = 'Message should be between 10 and 20 symbols';
    return errors;
  };
  render() {
    let { data } = this.state;
    let date = new Date(Date.now());
    let formateddate = `${date.getDay()}.${date.getMonth()}.${date.getFullYear()}`;
    if (this.props.sended) {
      return (
        <div className='feedback-form' id='feed-back'>
          <div className='feedback-form-titles'>
            <div className='feedback-form-titles__title'>
              {this.props.title}
            </div>
            <div className='feedback-form-titles__subtitle'>
              I hope you enjoyed it as like as me
            </div>
            <div className='feedback-form-titles__message'>
              If you still have any questions, have an offer for me or have an
              ideas for improving this site and my portfolio, please give me a
              feedback in the form below or using contacts at the{' '}
              <a href='#links' onClick={(e: any) => smoothScroll(e)}>
                top of a screen
              </a>
              :
            </div>
          </div>
          <div className='thanks'>
            Thank you for sending me a message, i`ll answer you as much fast as
            possible
          </div>
        </div>
      );
    }
    return (
      <div className='feedback-form' id='feed-back'>
        <div className='feedback-form-titles'>
          <div className='feedback-form-titles__title'>
            Thank you for wathing my resume
            <span
              style={{
                fontSize: '0.7em',
                margin: '0 0 10px 0',
                display: 'block',
                color: '#B00020'
              }}
            >
              (Form disabled now, but you can see some validation)
            </span>
          </div>
          <div className='feedback-form-titles__subtitle'>
            I hope you enjoyed it as like as me
          </div>
          <div className='feedback-form-titles__message'>
            If you still have any questions, have an offer for me or have an
            ideas for improving this site and my portfolio, please give me a
            feedback in the form below or using contacts at the{' '}
            <a href='#links' onClick={(e: any) => smoothScroll(e)}>
              top of a screen
            </a>
            :
          </div>
        </div>

        <form>
          <InputGroup>
            <Input
              label='Name...'
              type='text'
              name='name'
              onChange={this.onChange}
              error={this.state.errors.name}
            >
              {data.name}
            </Input>
            <Input
              label='Email...'
              type='email'
              name='contact'
              onChange={this.onChange}
              error={this.state.errors.contact}
            >
              {data.contact}
            </Input>
            <span>{`date: ${formateddate}`}</span>
          </InputGroup>
          <InputGroup>
            <TextArea
              label='Your message...'
              type='text'
              name='message'
              onChange={this.onChange}
              error={this.state.errors.message}
            >
              {data.message}
            </TextArea>
          </InputGroup>
          <button type='submit' onClick={this.onSubmit}>
            send
          </button>
        </form>
      </div>
    );
  }
}

export default FeedbackForm;
