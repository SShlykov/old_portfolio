import React from 'react';

import { connect } from 'react-redux';
import { sendToMail } from '../../actions/mailings';

import FeedbackForm from './../forms/feedbackForm';
import { IFormData } from '../forms/feedbackForm/FeedbackForm';
import Resume from './sergeyPage/Resume';

export interface IPageProps {
  children?: any;
  sendToMail: (data: IFormData, email: string) => Promise<void>;
}
class SergeyPage extends React.Component<IPageProps> {
  state = {
    isSended: false
  };
  submit = (data: IFormData) =>
    this.props.sendToMail(data, 'zeitment@gmail.com').then(() => {
      this.setState({ isSended: true });
    });
  render() {
    return (
      <>
        <div className='resume-header'>
          <div className='resume-header__name'>
            Sergey <span>Shlykov</span>
          </div>
          <div className='resume-header__position'>full-stack developer</div>
        </div>
        <Resume />
        <FeedbackForm
          submit={this.submit}
          sended={this.state.isSended}
          title={`Thank you for watching my resume`}
        />
      </>
    );
  }
}

export default connect(
  null,
  { sendToMail }
)(SergeyPage);
