import React from 'react';
import { useTranslation } from 'react-i18next';
import DataBlock from '../../containers/dataBlock';
import { IPageData } from '../mainPage/interfaces';

const Skills = () => {
  const { t } = useTranslation();
  const data: IPageData = t('resume data'),
    titles: { skills: { title: string } } = t('resume_blocks'),
    skills = data.skills;
  return (
    <DataBlock title={titles.skills.title} className=''>
      {Object.keys(skills).map((el, idx) => {
        return (
          <div className='skills-block' key={`skb-${idx}`}>
            <div className='skills-block__title'>
              {
                /*
              //@ts-ignore*/
                titles.skills[el]
              }
              :
            </div>
            <div className='skills-block-skills'>
              {/*
              //@ts-ignore */
              skills[el].map((skill, index) => {
                return (
                  <span
                    className='skills-block-skills__skill'
                    key={`skbss-${idx}-${index}`}
                  >
                    {skill}
                  </span>
                );
              })}
            </div>
          </div>
        );
      })}
    </DataBlock>
  );
};

export default Skills;
