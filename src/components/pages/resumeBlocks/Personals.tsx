import React from 'react';
import DataBlock from '../../containers/dataBlock/index';
import { useTranslation } from 'react-i18next';

export interface IPersonals {}

export interface IPersonalsProps {
  data: {
    personals: IPersonals;
  };
  titles: {
    personals: {
      title: string;
    };
  };
  className: string;
  photo?: string;
}

const Personals = (props: IPersonalsProps) => {
  const { t } = useTranslation();
  let photo: any;
  let heading = <h3 className='title'>{t('Personals')}</h3>;
  if (props.photo) {
    return (
      <DataBlock
        title={!props.photo ? props.titles.personals.title : ''}
        className={props.className}
      >
        <div>
          <img src={props.photo} alt=''></img>
          <ul className='personals'>
            {heading}
            {Object.keys(props.data.personals).map(el => {
              return (
                <li key={el}>
                  <span className='key'>
                    {
                      //@ts-ignore
                      props.titles.personals[el]
                    }
                    :
                  </span>
                  <span className='value'>
                    {
                      //@ts-ignore
                      props.data.personals[el]
                    }
                  </span>
                </li>
              );
            })}
          </ul>
        </div>
      </DataBlock>
    );
  }
  return (
    <DataBlock
      title={!photo ? props.titles.personals.title : ''}
      className={props.className}
    >
      <ul className='personals'>
        {Object.keys(props.data.personals).map(el => {
          return (
            <li key={el}>
              <span className='key'>
                {
                  //@ts-ignore
                  props.titles.personals[el]
                }
                :
              </span>
              <span className='value'>
                {
                  //@ts-ignore
                  props.data.personals[el]
                }
              </span>
            </li>
          );
        })}
      </ul>
    </DataBlock>
  );
};

export default Personals;
