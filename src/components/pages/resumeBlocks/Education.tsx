import React from 'react';
import DataBlock from '../../containers/dataBlock';
import { useTranslation } from 'react-i18next';
import { IPageData } from '../mainPage/interfaces';

const Education = ({ className }: { className: string }) => {
  const { t } = useTranslation();
  let titles: {
      education: { title: string; master: string; bachelor: string };
    } = t('resume_blocks'),
    data: IPageData = t('resume data');
  return (
    <DataBlock title={titles.education.title} className={className}>
      <ul className='education'>
        <li>
          <span className='education__level'>{titles.education.master}:</span>
          <span className='education__place'>{data.degree.master}</span>
        </li>
        <li>
          <span className='education__level'>{titles.education.bachelor}:</span>
          <span className='education__place'>{data.degree.bachelor}</span>
        </li>
      </ul>
    </DataBlock>
  );
};

export default Education;
