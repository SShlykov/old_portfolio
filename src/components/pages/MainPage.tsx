import React from 'react';
import { Container } from '../containers/container';
import Welcomescreen from './mainPage/Welcomescreen';
import Inspirationscreen from './mainPage/Inspirationscreen';
import ShortResume from './mainPage/ShResume';
import Projects from './mainPage/Projects';

import { connect } from 'react-redux';
import { sendToMail } from '../../actions/mailings';

import FeedbackForm from './../forms/feedbackForm';
import { IFormData } from '../forms/feedbackForm/FeedbackForm';
import { IPageProps } from './SergeyPage';

class MainPage extends React.Component<IPageProps> {
  state = {
    isSended: false
  };
  submit = (data: IFormData) =>
    this.props.sendToMail(data, 'zeitment@gmail.com').then(() => {
      this.setState({ isSended: true });
    });
  render() {
    return (
      <Container>
        <Welcomescreen />
        <Inspirationscreen />
        <ShortResume />
        <Projects />
        <FeedbackForm
          submit={this.submit}
          sended={this.state.isSended}
          title={`Thank you for watching my portfolio`}
        />
      </Container>
    );
  }
}

export default connect(
  null,
  { sendToMail }
)(MainPage);
