import React from 'react';
import { useTranslation } from 'react-i18next';

const Welcomescreen = () => {
  const { t } = useTranslation();
  return (
    <div className={`wscreen ${t('current locale')}`}>
      <div className='wscreen-titles'>
        <div className='title cap-top'>
          <div className='title__name'>{t('Shlykov')}</div>
          <div className='title__cap'>full-stack developer</div>
        </div>
        <div className='title cap-bottom'>
          <div className='title__name'>{t('Sergey')}</div>
          <div className='title__cap'>and a little bit more...</div>
        </div>
      </div>
    </div>
  );
};

export default Welcomescreen;
