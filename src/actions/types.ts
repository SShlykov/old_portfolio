import { FetchUserAction, ChangeLangAction } from './index';
export enum ActionTypes {
  fetchUser,
  sandMail,
  changeLang
}

export type Action = FetchUserAction | ChangeLangAction;
