import { Dispatch } from 'redux';
import { ActionTypes } from './types';

export interface ChangeLangAction {
  type: ActionTypes.changeLang;
  payload: string;
}

export const sendToMail = (lang: string) => {
  return (dispatch: Dispatch) => {
    dispatch<ChangeLangAction>({
      type: ActionTypes.changeLang,
      payload: lang
    });
  };
};
