export const smoothScroll = (e: any) => {
  e.preventDefault();
  let elem = document.querySelector(e.target.getAttribute('href'));
  if (elem) {
    window.scrollTo({
      behavior: 'smooth',
      //@ts-ignore
      top: elem.offsetTop
    });
  } else {
    console.warn(
      'Something went wrong, it looks like link you refers to doesn`t exist. Please write us about it'
    );
    window.scrollTo({ behavior: 'smooth', top: 0 });
  }
};

export const smoothScrollToId = (id: string) => {
  let elem = document.querySelector(`#${id}`);
  if (elem) {
    window.scrollTo({
      behavior: 'smooth',
      //@ts-ignore
      top: elem.offsetTop
    });
  } else {
    window.scrollTo({
      behavior: 'smooth',
      //@ts-ignore
      top: 0
    });
    console.warn(
      'Something went wrong, it looks like link you refers to doesn`t exist. Please write us about it'
    );
  }
};
