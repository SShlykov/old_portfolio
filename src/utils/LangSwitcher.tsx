import React from 'react';
import { useTranslation } from 'react-i18next';

const LangSwitcher = () => {
  const { t, i18n } = useTranslation();
  let swto: string = t('switch to').toLowerCase();
  return (
    <li onClick={() => i18n.changeLanguage(swto)}>
      <p>{swto.toUpperCase()}</p>
    </li>
  );
};

export default LangSwitcher;
