import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import { en, ru } from './locales';
import store from './store';

const resources = {
  en: {
    translation: en
  },
  ru: {
    translation: ru
  }
};

let state = store.getState();

i18n.use(initReactI18next).init({
  resources,
  lng: state.lng,
  keySeparator: false, // we do not use keys in form messages.welcome
  returnObjects: true,
  interpolation: {
    escapeValue: false // react already safes from xss
  }
});

export default i18n;
